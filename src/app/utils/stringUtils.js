import linksConfig from '../consts/config/linksConfig';

const formatString = (string, ...values) => {
    for (let i = 0; i < values.length; i++) {
        string = string.replace(`{${0}}`, values[i]);
    }

    const link = linksConfig.defaultLinks + string;

    return link;
};

export default {formatString,};