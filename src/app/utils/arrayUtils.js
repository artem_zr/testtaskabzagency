const toArray = (a) => (a ? (Array.isArray(a) ? a : [a]) : []);

export default {toArray,}