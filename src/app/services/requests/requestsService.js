import server from '../server/server';
import linksConfig from '../../consts/config/linksConfig';
import stringUtils from '../../utils/stringUtils';

const getAccessToken = async () => {
    const linkPath = stringUtils.formatString(linksConfig.linksPath.accessToken);
    const accessTokenData = await server.get(linkPath);

    localStorage.setItem("accessToken", accessTokenData.data.token);
};

const getUsers = async (pageCount, userCount) => {
    const usersDataReference = linksConfig.linksPath.usersData;
    const linkPath = stringUtils.formatString(usersDataReference, pageCount, userCount);

    return await server.get(linkPath);
};

const getUsersByLink = async (linkPath) => {
    const usersData = await server.get(linkPath);

    return usersData;
};

const getPositions = async () => {
    return await server.get(stringUtils.formatString(linksConfig.linksPath.userPositions));
};

export default {
    getAccessToken,
    getUsers,
    getUsersByLink,
    getPositions,
}