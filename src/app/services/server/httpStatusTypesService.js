import * as httpErrorsTypes from '../../consts/errors/httpErrorsTypes';

const isNotAuthorized = (e) => e?.response?.status === httpErrorsTypes.NOT_AUTHORIZED;

export default {
    isNotAuthorized,
};
