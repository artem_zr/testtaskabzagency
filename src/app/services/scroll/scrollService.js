import { Link, animateScroll as scroll } from "react-scroll";

const scrollToRegister = () => {
    const registerSection = document.getElementById('register-section');
    
    registerSection.scrollIntoView(); 
}

export default {scrollToRegister};