import * as commonLayoutSizes from '../../consts/layout/commonLayoutSizes';
import * as commonLayoutTypes from '../../consts/layout/commonLayoutTypes';

const getSize = () => ({
    width: window.innerWidth,
    height: window.innerHeight,
});

const getLayout = (deviceWidth) => {
    if (deviceWidth <= commonLayoutSizes.MOBILE) {
        return commonLayoutTypes.MOBILE;
    } else if (deviceWidth >= commonLayoutSizes.DESKTOP) {
        return commonLayoutTypes.DESKTOP;
    } else {
        return commonLayoutTypes.TABLET;
    }
};

export default {getSize, getLayout};

