export default {
    defaultLinks: 'https://frontend-test-assignment-api.abz.agency/api/v1/',
    linksPath: {
        accessToken: 'token',
        usersData: 'users?page={0}&count={0}',
        usersRegistration: 'users',
        userDataById: 'users/{0}',
        userPositions: 'positions'
    }
}