export const NOT_AUTHORIZED = 401;
export const NOT_FOUND = 404;
export const BAD_REQUEST = 400;
export const TOO_MANY_REQUESTS = 429;
