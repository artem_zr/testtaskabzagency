import React from 'react';

import styles from './RadioButton.module.scss';

const RadioButton = (props) => {
    const {onChange, name, checked, value, className} = props;

    return (
        <div className={styles.MainContainer}>
            <input
                value={value}
                checked={checked}
                onChange={onChange}
                type="radio"
            />
            <span className={`ica--txt-p ${styles.Label}`}>{name}</span>
        </div>
    );
};

export default RadioButton;