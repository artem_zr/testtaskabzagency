import React from 'react';

import Button from '../Button';
import styles from './ButtonText.module.scss';

const ButtonText = ({className, ...props}) => {
    return (
        <Button {...props} className={`${styles.ButtonText} ${className}`} />
    );
}

export default ButtonText;