import React from 'react';

import Button from '../Button';
import styles from './ButtonColor.module.scss';

const ButtonColor = (props) => {
    return <Button {...props} className={`${props.className} ${styles.ButtonColor}`} />;
};

export default ButtonColor;