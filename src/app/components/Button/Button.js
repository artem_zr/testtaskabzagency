import React from 'react';

import scrollService from '../../services/scroll/scrollService';
import styles from './Button.module.scss';

const Button = (props) => {
    const {className, onClick, onChange, children} = props;

    const onButtonClick = onClick || scrollService.scrollToRegister;

    return (
        <button
            className={`${styles.Button} ${className}`}
            onClick={onButtonClick}
            onChange={onChange}
        >
            {children}
        </button>
    );
};

export default Button;