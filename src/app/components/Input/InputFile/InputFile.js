import React, { useRef } from 'react';

import styles from './InputFile.module.scss';
import Button from '../../Button/Button';

const InputFile = ({accept='image/jpeg', className, onChange, buttonText, placeHolder}) => {
    const inputField = useRef(null);

    const onInputButtonClick = () => {
        inputField.current.click();
    };

    return (
        <>
            <input ref={inputField} type='file' accept={accept} multiple className={styles.HiddenInput} onChange={onChange} />
            <div className={`${styles.InputField} ${className}`}>
                <span className={styles.PlaceHolderText}>{placeHolder}</span>
                <Button onClick={onInputButtonClick} className={styles.Button} children={buttonText} />
            </div>
        </>   
    );
};

export default InputFile;