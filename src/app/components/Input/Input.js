import classNames from 'classnames';
import React from 'react';

import styles from './Input.module.scss';

const Input = (props) => {
    const {type, className, value, maxLength, onChange, placeholder, accept, ref} = props;
    const inputClassName = classNames(styles.Input, className);

    return (
        <input
            className={inputClassName}
            maxLength={maxLength}
            onChange={onChange}
            placeholder={placeholder}
            type={type}
            accept={accept}
            value={value}
            ref={ref}
        />
    );
};

export default Input;
