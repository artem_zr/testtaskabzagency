import React from 'react';

import Input from '../Input';

const InputText = ({type='text', ...props}) => {
    return (
        <Input type={type} {...props}/>
    );
};

export default InputText;