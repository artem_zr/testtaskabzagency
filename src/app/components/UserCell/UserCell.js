import React from 'react';

import styles from './UserCell.module.scss';
import Image from '../Image/Image';

const UserCell = ({onClick, imgUrls, name, position, email, phone}) => {
    const editPhoneNumber = [phone.slice(0, 4), phone.slice(4,6), phone.slice(6,9), phone.slice(9,11), phone.slice(11,13)].join(' ');

    return (
        <div className={styles.CellContainer} onClick={onClick}>
            {imgUrls && <Image src={imgUrls} className={styles.Image}/>}
            <div className={`ica--txt-h2 ${styles.NameText}`}>{name}</div>
            <span className={`ica--txt-p ${styles.PositionText}`}>{position}</span>
            <span className={`ica--txt-p ${styles.Email}`}>{email}</span>
            <span className={`ica--txt-p ${styles.Phone}`}>{editPhoneNumber}</span>
        </div>
    );
};

export default UserCell;