import React, { useEffect, useState } from 'react';

import styles from './UserSection.module.scss';
import textData from '../../consts/textData/textData.json';
import UserCell from '../../components/UserCell/UserCell';
import requestsService from '../../services/requests/requestsService';
import ButtonColor from '../../components/Button/ButtonColor/ButtonColor';
import layoutService from '../../services/layout/layoutService';

const UserSection = (props) => {
    const {layout} = props;
    const isMobileLayout = layoutService.isMobileLayout(layout);

    const [userData, setUserData] = useState({
        users: null,
        nextUrl: null
    });
    const [isShowMoreButtonVisible, setIsShowMoreButtonVisible] = useState(true);

    useEffect(async () => {
        const startPageCount = 1;
        const startUsersCount = isMobileLayout ? 3 : 6;

        const usersCollection = await requestsService.getUsers(startPageCount, startUsersCount);

        setUserData({users: usersCollection.data.users, nextUrl: usersCollection.data.links.next_url});
    }, []);

    const getMoreUsers = async () => {
        const nextUserData = await requestsService.getUsersByLink(userData.nextUrl);
        
        const userCollection = userData.users.concat(nextUserData.data.users);
        const nextUrl = nextUserData.data.links.next_url;

        setIsShowMoreButtonVisible(nextUrl);
        setUserData({users: userCollection, nextUrl: nextUrl});
    };
    
    const UserCellCollection = () => {
        return (
            <>
                {userData.users?.map((item, index) => {
                    const imgUrls = item.photo;
                    const name = item.name;
                    const position = item.position;
                    const email = item.email;
                    const phone = item.phone;

                    return (
                        <UserCell
                            imgUrls={imgUrls}
                            name={name}
                            position={position}
                            email={email}
                            phone={phone}
                            key={index.toString()}
                        />
                    );
                })}
            </>
        );
    };

    return (
        <div className={styles.UserSectionContainer}>
            <div className={styles.TextContainer}>
                <span className={`ica--txt-h1`}>{textData.TITLE_TEXT_USER}</span>
                <span className={`ica--txt-p ${styles.AttentionText}`}>{textData.ATTENTION_TEXT_USER}</span>
            </div>
            <div className={styles.UserCellContainer}>
                <UserCellCollection />
            </div>
            {isShowMoreButtonVisible && (
                <ButtonColor className={styles.Button} onClick={getMoreUsers}> 
                    {textData.BUTTON_TEXT_USER}
                </ButtonColor>
            )}
        </div>
    );
};

export default UserSection;