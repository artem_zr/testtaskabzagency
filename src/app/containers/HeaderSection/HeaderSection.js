import React, { useState } from 'react';

import ButtonText from '../../components/Button/ButtonText/ButtonText';
import Image from '../../components/Image/Image';
import styles from './HeaderSection.module.scss';
import logoImage from '../../../assets/images/logo/logo.svg';
import buttonImage from '../../../assets/images/button/menu-button.svg';
import layoutService from '../../services/layout/layoutService';
import headerButtonsData from './headerButtonsData';

const HeaderSection = ({layout}) => {
  const isDesktopLayout = layoutService.isDesktopLayout(layout);

  return (
    <div className={styles.HeaderContainer}>
        <Image src={logoImage} className={styles.Logo}/>
        <div className={styles.ButtonContainer}>
          {isDesktopLayout && (
            <>
              {headerButtonsData.map((button, index) => {
                const name = button.name;

                return (
                  <ButtonText
                    children={name}
                    key={index.toString()}
                  />
                );
              })}
            </>
          )}
          {!isDesktopLayout && (
            <Image src={buttonImage} className={styles.MenuButton}/>
          )}
        </div>
        
    </div>
  );
}

export default HeaderSection;
