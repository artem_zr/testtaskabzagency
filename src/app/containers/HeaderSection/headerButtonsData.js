import textData from '../../consts/textData/textData.json';

export default [
    {
        name: textData.ABOUT_ME_HEADER_BUTTON,
    },
    {
        name: textData.RELATIONSHIPS_HEADER_BUTTON,
    },
    {
        name: textData.REQUIREMENTS_HEADER_BUTTON,
    },
    {
        name: textData.USERS_HEADER_BUTTON,
    },
    {
        name: textData.SIGN_UP_HEADER_BUTTON,
    }
]