import React, { useEffect, useState } from 'react';

import windowResizeService from '../../services/layout/windowResizeService';
import AcquaintanceSection from '../AcquaintanceSection/AcquaintanceSection';
import BannerSection from '../BannerSection/BannerSection';
import HeaderSection from '../HeaderSection/HeaderSection';
import RegisterSection from '../RegisterSection/RegisterSection';
import UserSection from '../UsersSection/UserSection';
import styles from './App.module.scss';

const App = () => {
  const [windowSize, setWindowSize] = useState(windowResizeService.getSize());

  useEffect(() => {
      const handleResize = () => {
          setWindowSize(windowResizeService.getSize());
      };

      window.addEventListener('resize', handleResize);
      return () => window.removeEventListener('resize', handleResize);
  }, []);

  const layout = windowResizeService.getLayout(windowSize.width);

  return (
    <div className={styles.MainPage}>
      <HeaderSection layout={layout} />
      <BannerSection />
      <AcquaintanceSection />
      <UserSection layout={layout} />
      <RegisterSection id="register-section" layout={layout}/>
    </div>
  );
}

export default App;
