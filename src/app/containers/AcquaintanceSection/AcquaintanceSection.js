import React from 'react';


import styles from './AcquaintanceSection.module.scss';
import Image from '../../components/Image/Image';
import acquaintanceImage from '../../../assets/images/picture/acquaintance.svg'
import textData from '../../consts/textData/textData.json';
import ButtonText from '../../components/Button/ButtonText/ButtonText';

const AcquaintanceSection = () => {
    return (
        <div className={styles.AcquaintanceContainer}>
            <div className={`ica--txt-h1`}>{textData.TITLE_TEXT_ACQUAINTANCE}</div>
            <div className={styles.DescriptionContainer}>
                <Image src={acquaintanceImage} className={styles.Image}/>
                <div className={styles.DescriptionTextContainer}>
                    <span className={`ica--txt-h2 ${styles.HeadingText}`}>{textData.HEADING_TEXT_ACQUAINTANCE}</span>
                    <span className={`ica--txt-p ${styles.FirstDescriptionText}`}>{textData.FIRST_DESCRIPTION_TEXT_ACQUAINTANCE}</span>
                    <span className={`ica--txt-p ${styles.SecondDescriptionText}`}>{textData.SECOND_DESCRIPTION_TEXT_ACQUAINTANCE}</span>
                    <ButtonText className={styles.Button} >
                        {textData.SING_UP_NOW_BUTTON}
                    </ButtonText>
                </div>
            </div>
        </div>
    );
};

export default AcquaintanceSection;
