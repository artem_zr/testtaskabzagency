import React, {useState} from 'react';

import textData from '../../../consts/textData/textData.json';
import registerSectionService from '../registerSectionService';
import InputTextComponent from './InputTextComponent/InputTextComponent';
import layoutService from '../../../services/layout/layoutService';

const InputFieldCollection = ({layout}) => {
    const isMobileLayout = layoutService.isMobileLayout(layout);
    const isTabletLayout = layoutService.isTabletLayout(layout);

    const phonePromptText = isMobileLayout ? textData.PHONE_INPUT_PROMPT_TEXT_REGISTER_MOBILE : 
        isTabletLayout ? textData.PHONE_INPUT_PROMPT_TEXT_REGISTER_TABLET : 
        textData.PHONE_INPUT_PROMPT_TEXT_REGISTER_DESKTOP;

    const [registerData, setRegisterData] = useState({
        name: null,
        email: null,
        phone: null
    });

    const [isInputError, setIsInputError] = useState({
        isNameError: false,
        isEmailError: false,
        isPhoneError: false
    });

    const onNameFieldChange = ({target}) => {
        const isValidName = registerSectionService.isValidNameField(target.value);

        setRegisterData({...registerData, name: isValidName ? target.value : null});
        setIsInputError({...isInputError, isNameError: !isValidName});
    };

    const onEmailFieldChange = ({target}) => {
        const isValidEmail = registerSectionService.isValidEmailField(target.value);

        setRegisterData({...registerData, email: isValidEmail ? target.value : null});
        setIsInputError({...isInputError, isEmailError: !isValidEmail});
    };

    const onPhoneFieldChange = ({target}) => {
        const isValidPhone = registerSectionService.isValidPhoneField(target.value);

        setRegisterData({...registerData, phone: isValidPhone ? target.value : null});
        setIsInputError({...isInputError, isPhoneError: !isValidPhone});
    };

    return (
        <>
            <InputTextComponent
                labelText={textData.NAME_INPUT_LABEL_REGISTER}
                maxLength={60}
                onChange={onNameFieldChange}
                placeholderText={textData.NAME_INPUT_PLACEHOLDER_REGISTER}
                isInputError={isInputError.isNameError}
            />
            <InputTextComponent
                labelText={textData.EMAIL_INPUT_LABEL_REGISTER}
                maxLength={60}
                onChange={onEmailFieldChange}
                placeholderText={textData.EMAIL_INPUT_PLACEHOLDER_REGISTER}
                isInputError={isInputError.isEmailError}
            />
            <InputTextComponent
                labelText={textData.PHONE_INPUT_LABEL_REGISTER}
                maxLength={13}
                onChange={onPhoneFieldChange}
                placeholderText={textData.PHONE_INPUT_PLACEHOLDER_REGISTER}
                isInputError={isInputError.isPhoneError}
                isPhoneInputField
                promptText={phonePromptText}
            />
        </>
    );
}

export default InputFieldCollection;