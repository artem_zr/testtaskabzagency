import React from 'react';

import styles from '../../RegisterSection.module.scss';
import textData from '../../../../consts/textData/textData.json';
import InputText from '../../../../components/Input/InputText/InputText';

const InputTextComponent = (props) => {
    const {labelText, maxLength, onChange, placeholderText, isInputError, isPhoneInputField, promptText} = props;

    return (
        <div className={styles.InputContainer}>
                <span className={`ica--txt-p ${styles.InputLabel}`}>{labelText}</span>
                <InputText
                    maxLength={maxLength}
                    onChange={onChange}
                    placeholder={placeholderText}
                    className={`${styles.InputField} ${isInputError && styles.InputError}`}
                />
                {isInputError && (
                   <span className={`ica--txt-p ${styles.ErrorText}`}>{textData.ERROR_TEXT_REGISTER}</span> 
                )}
                {isPhoneInputField && (
                    <span className={`ica--txt-p ${styles.PromptPhoneText}`}>{promptText}</span>
                )}
        </div>
    );
};

export default InputTextComponent;