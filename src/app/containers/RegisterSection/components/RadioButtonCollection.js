import React from 'react';

import RadioButton from '../../../components/RadioButton/RadioButton';

const RadioButtonCollection = ({positions, onChange, checkedPosition, className}) => {
    return (
        <>
            {positions?.map((item, index) => {
                const name = item.name;

                return (
                    <div>
                        <RadioButton
                            onChange={onChange}
                            checked={checkedPosition == name}
                            name={name}
                            value={name}
                            key={index.toString()}
                            className={className}
                        />
                    </div>
                );
            })}
        </>
    );
};

export default RadioButtonCollection;