const isValidNameField = (value) => {
    if (value.length <= 1) {
        return false;
    }
    return true;
}

//In the case of a more complex project, the regular expressions should be placed in a separate configuration file

const isValidEmailField = (value) => {
    const emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    
    return emailRegex.test(value);
};

const isValidPhoneField = (value) => {
    const phoneRegex = /[\+]{1}380([0-9]{9})/;

    return phoneRegex.test(value);
};

export default {
    isValidNameField,
    isValidEmailField,
    isValidPhoneField,
}