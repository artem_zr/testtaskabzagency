import React, {useEffect, useState} from 'react';

import styles from './RegisterSection.module.scss';
import textData from '../../consts/textData/textData.json';
import requestsService from '../../services/requests/requestsService';
import InputFile from '../../components/Input/InputFile/InputFile';
import RadioButtonCollection from './components/RadioButtonCollection';
import ButtonColor from '../../components/Button/ButtonColor/ButtonColor';
import InputFieldCollection from './components/InputFieldCollection';

const RegisterSection = (props) => {
    const {id, layout} = props;
    const [availablePositions, setAvailablePositions] = useState(null);
    const [chekedPosition, setChekedPosition] = useState(null);

    useEffect(async () => {
        await requestsService.getAccessToken();

        const positions = await requestsService.getPositions();

        setAvailablePositions(positions.data.positions);
    }, []);

    const positionChange = (event) => {
        setChekedPosition(event.target.value);
    };

    const inputFileFieldChange = (e) => {
        console.log(e.target.value);
    };

    return (
        <div className={styles.RegisterContainer} id={id}>
            <div className={styles.HeaderContainer}>
                <span className={`ica--txt-h1`}>{textData.TITLE_TEXT_REGISTER}</span>
            </div>
            <div className={styles.MainContainer}>
                <span className={`ica--txt-p ${styles.AttentionText}`}>{textData.ATTENTION_TEXT_REGISTER}</span>
                <InputFieldCollection layout={layout}/>
                <div className={styles.PositionsContainer}>
                    <span className={`ica--txt-p ${styles.PositionLabel}`}>{textData.POSITION_RADIOBUTTON_LABEL_REGISTER}</span>
                    <div className={styles.RadioButtonContainer}>
                        <RadioButtonCollection 
                            positions={availablePositions} 
                            onChange={positionChange} 
                            checkedPosition={chekedPosition}
                        />
                    </div>
                </div>
                <div className={styles.InputContainer}>
                    <span className={`ica--txt-p ${styles.InputLabel}`}>{textData.PHOTO_INPUT_LABEL_REGISTER}</span>
                    <InputFile
                        buttonText={textData.PHOTO_INPUT_BUTTON_REGISTER}
                        placeHolder={textData.PHOTO_INPUT_PLACEHOLDER_REGISTER}
                        className={styles.InputField}
                        onChange={inputFileFieldChange}
                    />
                </div>
                <ButtonColor className={styles.Button}> 
                    {textData.BUTTON_TEXT_REGISTER}
                </ButtonColor>
            </div>
        </div>
    );
};

export default RegisterSection;