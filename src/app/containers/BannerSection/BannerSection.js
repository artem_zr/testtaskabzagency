import React from 'react';

import bannerImage from '../../../assets/images/background/banner.jpg';
import ButtonColor from '../../components/Button/ButtonColor/ButtonColor';
import textData from '../../consts/textData/textData.json';
import Image from '../../components/Image/Image';
import styles from './BannerSection.module.scss';

const BannerSection = () => {
    return (
        <div className={styles.BannerContainer}>
            <Image src={bannerImage} className={styles.BannerImage}/>
            <div className={styles.ContentContainer}>
                <span className={`ica--txt-h1 ${styles.TitleText}`}>{textData.TITLE_TEXT_BANNER}</span>
                <span className={`ica--txt-p ${styles.DescriptionText}`}>{textData.DESCRIPTION_TEXT_BANNER}</span>
                <ButtonColor children={textData.SING_UP_NOW_BUTTON} className={styles.SingUpButton}/>
            </div>
        </div>
    );
};

export default BannerSection;