import './assets/scss/typography.scss';
import './assets/css/variables.css';
import './assets/css/index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/containers/App/App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);